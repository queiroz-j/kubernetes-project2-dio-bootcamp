######################################################################Project Kubernetes with simple backend, frontend, docker,kubernetes and database in gitlabci. Bootcamp- #DIO-CloudDevOpsExperience-2022 #By-jcqueiroz
######################################################################

Please start the project execute script.sh file.

######################################################################

<p align="center">
  <img src="/IMG/all-pods-cluster-project-ok.png" width="350" title="all-pods-cluster-project-ok">
  <img src="/IMG/services-ingress-gke-project.png" width="350" alt="services-ingress-gke-project">
  <img src="/IMG/site-frontend-form-project.png" width="350" alt="site-frontend-form-project">
  <img src="/IMG/workloads-gke-project.png" width="350" alt="workloads-gke-project">
</p>  